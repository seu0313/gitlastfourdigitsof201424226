#include <stdio.h>
// merge
int isprime(int);
int main(){
	int i,cnt;
	
	puts("Odd\n");
	for(i=1,cnt=0;i<=100;i++){
		if(i%2!=0){
			printf("%d\t",i);
			cnt++;
		}
		if(cnt%5==0)
			printf("\n");
	}
	puts("Even");
	for(i=1,cnt=0;i<=100;i++){
		if(i%2==0){
			printf("%d\t",i);
			cnt++;
		}
		if(cnt%5==0)
			printf("\n");
	}
	puts("\nPrime");
	for(i=1,cnt=0;i<=100;i++){
		if(isprime(i)&&i!=1){
			printf("%d\t",i);
		}
	}
	printf("\n");
	return 0;
}

int isprime(int i){
	int j;
	for(j=2;j<i;++j){
		if(i%j==0)
			return 0;
	}
	return 1;
}
